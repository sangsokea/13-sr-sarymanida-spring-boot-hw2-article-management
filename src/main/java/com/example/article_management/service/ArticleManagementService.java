package com.example.article_management.service;

import com.example.article_management.ArticleManagement;
import com.example.article_management.model.ArticleManagementM;
import com.example.article_management.repository.ArticleManagementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleManagementService implements ArticleManagement {

    @Autowired
    ArticleManagementRepository articleManagementRepository;


    @Override
    public List<ArticleManagementM> getAllArticleManagement() {
        return articleManagementRepository.getAllArticle();
    }

    @Override
    public void addAllArticleManagement(ArticleManagementM articleManagementM) {
        articleManagementRepository.addArticle(articleManagementM);
    }

    @Override
    public ArticleManagementM findArticleById(int id) {
        return articleManagementRepository.findArticleByID(id);
    }

    @Override
    public void deleteArticleById(int id) {
         articleManagementRepository.deleteArticleById(id);
    }

    @Override
    public void updateArticle(ArticleManagementM articleManagementM) {
        articleManagementRepository.updateArticleById(articleManagementM);
    }


//    @Override
//    public ArticleManagementM findById(int id) {
//        return articleManagementRepository.findArticleByID();
//    }


}
