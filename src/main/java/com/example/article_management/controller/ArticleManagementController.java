package com.example.article_management.controller;

import com.example.article_management.ArticleManagement;
import com.example.article_management.model.ArticleManagementM;
import com.example.article_management.repository.ArticleManagementRepository;
import com.example.article_management.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.IOException;

@Controller
public class ArticleManagementController
{

    @Autowired


    ArticleManagement articleManagement;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){
        model.addAttribute("articleManagement",new ArticleManagementRepository());
        model.addAttribute("Management",articleManagement.getAllArticleManagement());
        return "index";
    }

    @GetMapping("/form_add")
    public String formAdd(Model model){
//        ពេល Input data by keyboard វា stored ទុកក្នុង new ArticleManagementM()
//        បន្ទាប់មវាបោះចូល @ModelAttribute ArticleManagementM articleManagementM
        model.addAttribute("formAdd",new ArticleManagementM());
        return "form_add";
    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute("formAdd") @Valid ArticleManagementM formAdd, BindingResult bindingResult,Model model) {

//        model.addAttribute("formAdd",new ArticleManagementM());
        if (formAdd.getFile().isEmpty()) {
            // set default profileImage ;
            String s = "http://localhost:8080/images/noImage.jpg";
            formAdd.setProfile(s);
        } else {
            try {
                String filename = "http://localhost:8080/images/" + fileStorageService.saveFile(formAdd.getFile());
                formAdd.setProfile(filename);
            } catch (IOException ex) {
                System.out.println("Error with the image upload " + ex.getMessage());
            }
        }
        if (bindingResult.hasErrors()) {
                return "form_add";
        }
        articleManagement.addAllArticleManagement(formAdd);
        return  "redirect:/";
    }

    @GetMapping("/view_page/{id}")
    public String viewArticleById(@PathVariable int id, Model model){
        ArticleManagementM resultArticle = articleManagement.findArticleById(id);
        System.out.println("result student : "+ resultArticle);
        // send value to the view
        model.addAttribute("article",resultArticle);
        return "view_page";
    }

    @GetMapping(value = "/delete/{id}")
    public String deleteArticle(@PathVariable int id){
        articleManagement.deleteArticleById(id);
        return "redirect:/";
    }

    @PostMapping("/handle-update/{id}")
    public String handleUpdate(@ModelAttribute @Valid ArticleManagementM articleManagementM, BindingResult bindingResult, @PathVariable int id){
        if (bindingResult.hasErrors()) {
            return "update_form";
        }
        ArticleManagementM updateArticle =articleManagement.findArticleById(id);

        updateArticle.setTitle(articleManagementM.getTitle());
        updateArticle.setDescription(articleManagementM.getDescription());

        if(!articleManagementM.getFile().isEmpty()){
            try {
                String filename = "http://localhost:8080/images/" + fileStorageService.saveFile(articleManagementM.getFile());
                updateArticle.setFile(articleManagementM.getFile());
                updateArticle.setProfile(filename);
            } catch (IOException ex) {
                System.out.println("Error with the image upload " + ex.getMessage());
            }
        }

        articleManagement.updateArticle(updateArticle);

        return "redirect:/";
    }

    @GetMapping("/article-update/{id}")
    public String updateArticle(@PathVariable int id, Model model) {
        ArticleManagementM article = articleManagement.findArticleById(id);
        model.addAttribute ( "article", article );
        return "update_form";
    }

}
