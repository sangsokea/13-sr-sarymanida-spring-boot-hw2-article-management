package com.example.article_management;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStorage {

    public String saveFile(MultipartFile file) throws IOException;
}
